<!DOCTYPE html>
<html lang="en">
<head>

  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://localhost/finance/assets/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<form action= "<?php echo base_url;?>welcome/dealer_details" method="post">
  <div class="container-fluid col-md-6">
  <div class="form-group col-md-6">
  
    <label for="Onboardedby">Onboarded By</label>
    <input type="text" class="form-control" id="onboarded" name="onboarded" aria-describedby="emailHelp" placeholder="Onboarded By" required>
  </div>
  <div class="form-group col-md-6">
    <label for="companyname">Company Name</label>
    <input type="text" class="form-control" id="companyname" name="companyname" aria-describedby="companyname" placeholder="Company Name" maxlength="40" required>
  </div>
   <div class="form-group">
    <label for="inputAddress" class="col-form-label">Address</label>
    <input type="text" class="form-control" id="inputAddress" name="inputAddress" placeholder="1234 Main St" required>
  </div>
  <div class="form-group">
    <label for="inputAddress2" class="col-form-label">Address 2</label>
    <input type="text" class="form-control" id="inputAddress2" name="inputAddress2" placeholder="Apartment, studio, or floor" required>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity" class="col-form-label">City</label>
      <input type="text" class="form-control" id="inputCity" name="inputCity" required>
    </div>
       <div class="form-group col-md-6">
      <label for="inputState" class="col-form-label">State</label>
      <select id="inputState" class="form-control" name="inputState" required>

<option value="">------------Select State------------</option>
<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
<option value="Andhra Pradesh">Andhra Pradesh</option>
<option value="Arunachal Pradesh">Arunachal Pradesh</option>
<option value="Assam">Assam</option>
<option value="Bihar">Bihar</option>
<option value="Chandigarh">Chandigarh</option>
<option value="Chhattisgarh">Chhattisgarh</option>
<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
<option value="Daman and Diu">Daman and Diu</option>
<option value="Delhi">Delhi</option>
<option value="Goa">Goa</option>
<option value="Gujarat">Gujarat</option>
<option value="Haryana">Haryana</option>
<option value="Himachal Pradesh">Himachal Pradesh</option>
<option value="Jammu and Kashmir">Jammu and Kashmir</option>
<option value="Jharkhand">Jharkhand</option>
<option value="Karnataka">Karnataka</option>
<option value="Kerala">Kerala</option>
<option value="Lakshadweep">Lakshadweep</option>
<option value="Madhya Pradesh">Madhya Pradesh</option>
<option value="Maharashtra">Maharashtra</option>
<option value="Manipur">Manipur</option>
<option value="Meghalaya">Meghalaya</option>
<option value="Mizoram">Mizoram</option>
<option value="Nagaland">Nagaland</option>
<option value="Orissa">Orissa</option>
<option value="Pondicherry">Pondicherry</option>
<option value="Punjab">Punjab</option>
<option value="Rajasthan">Rajasthan</option>
<option value="Sikkim">Sikkim</option>
<option value="Tamil Nadu">Tamil Nadu</option>
<option value="Tripura">Tripura</option>
<option value="Uttaranchal">Uttaranchal</option>
<option value="Uttar Pradesh">Uttar Pradesh</option>
<option value="West Bengal">West Bengal</option>
</select>
    </div>
    <div class="form-group col-md-6">
      <label for="inputZip" class="col-form-label">Zip</label>
      <input type="Zip" class="form-control" id="inputZip" name="inputZip" required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputLocation" class="col-form-label">Location</label>
      <input type="text" class="form-control" id="inputLocation" name="inputLocation" required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputcontact" class="col-form-label">Contact Person</label>
      <input type="text" class="form-control" id="inputContact" name="inputContact" required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputEmail4" class="col-form-label">Contact Email</label>
      <input type="email" class="form-control" id="inputEmail4" name="inputEmail4" placeholder=" Contact Email" required>
    </div>
  
   <div class="form-group col-md-6">
      <label for="inputsubscription" class="col-form-label">subscription type</label>
      <input type="text" class="form-control" id="inputsubscription" name="inputsubscription" placeholder=" subscription Type" required>
    </div>
     <div class="form-group col-md-6">
      <label for="inputpackage" class="col-form-label">Package type</label>
      <input type="text" class="form-control" id="inputpackage" name="inputpackage" placeholder=" package Type" required>
    </div>
     <div class="form-group col-md-6">
      <label for="inputAmount" class="col-form-label">Amount paid</label>
      <input type="number" class="form-control" id="inputAmount" name="inputAmount" placeholder=" Enter Amount Paid" required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputDate" class="col-form-label">Date</label>
      <input type="date" class="form-control" id="inputDate2" name="inputDate2" placeholder=" Enter Date" required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputDatareceived" class="col-form-label">Data received</label>
      <input type="text" class="form-control" id="inputDatareceived" name="inputDatareceived" placeholder="Data Received or not" required>
    </div>
     <div class="form-group col-md-6">
      <label for="inputstartsfrom" class="col-form-label">Package starts from</label>
      <input type="date" class="form-control" id="inputstartsfrom" name="inputstartsfrom" placeholder="Package starts from" required>
    </div>
     <div class="form-group col-md-6">
      <label for="inputPaymentId" class="col-form-label">Payment ID</label>
      <input type="text" class="form-control" id="inputpaymentId" name="inputPaymentId" placeholder="payment ID" required>
    </div>
     <div class="form-group col-md-6">
      <label for="inputPaymentdetails" class="col-form-label">Payment details</label>
      <input type="text" class="form-control" id="inputPaymentdetails" name="inputPaymentdetails" placeholder="payment details" required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputRemarks" class="col-form-label">Privileage Remarks</label>
      <input type="text" class="form-control" id="inputRemarks" name="inputRemarks" placeholder="Privileage Remarks" required>
    
    </div>
    <div class="form-group col-md-6">
      <label for="inputordercount" class="col-form-label">Order Count</label>
      <input type="text" class="form-control" id="inputordercount" name="inputordercount" placeholder="Order Count" required>
    </div> 
    <button type="submit" class="btn btn-primary form-group button-must">Submit</button> 
</form>
  </div>

</body>
</html>